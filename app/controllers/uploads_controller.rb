class UploadsController < ApplicationController

  def create
    params[:files].each{ |file|
      @upload = Upload.create(file: file)
      if @upload.save
        respond_to do |format|
          format.html { render json: @upload.to_jq_upload, content_type: 'text/html', layout: false }
          format.json { render json: @upload.to_jq_upload }
        end
      else
        render json: { error: @upload.errors.full_messages }, status: 304
      end
    }
    
  end

  def destroy
  end


end

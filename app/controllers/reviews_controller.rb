class ReviewsController < ApplicationController

  def create
    #Get all the attributes from previous review of this material
    material = Material.find(params[:material][:id])
    ratings = []
    times = []
    grade_ranges = []
    if material.reviews
      prev_reviews = material.reviews
      num_reviews = prev_reviews.length
      prev_reviews.each do |review|
        if review.overall_rating
          ratings.push(review.overall_rating)
        end
        if review.time_required
          times.push(review.time_required)
        end
        if review.grade_level_top && review.grade_level_bottom
          grade_ranges.push([review.grade_level_bottom, review.grade_level_top])
        end
      end
    end

    #create the new review
    @review = material.reviews.build(review_params)
    @review.user_id = current_user.id
    if @review.save
      # Add new reviews attributes to array of all review attributes
      if @review.overall_rating
        ratings.push(@review.overall_rating)
      end
      if @review.time_required && !@review.time_required.blank?
        times.push(@review.time_required)
      else
        times.push(material.time_required)
      end
      if @review.grade_level_bottom && !@review.grade_level_bottom.blank?
        grade_ranges.push([@review.grade_level_bottom, @review.grade_level_top])
      end

      #Create new attributes for material by average attributes from all its reviews
      new_rating = (ratings.reduce(:+).to_f / ratings.length).round(2)
      new_time = times.reduce(:+) / times.length
      tops = []
      bottoms = []
      grade_ranges.each do |range|
        bottoms.push(range[0])
        tops.push(range[1])
      end
      new_grade_range = Range.new((bottoms.reduce(:+).to_f / bottoms.length).round, (tops.reduce(:+).to_f / tops.length).round)

      # Update the attributes of the material to take into account attributes from the new review
      material.update_attributes(time_required: new_time, average_rating: new_rating, grade_range: new_grade_range)

      redirect_to material
    else
      render '/'
    end
  end

    def new
    @material = Material.find(params[:material_id])
    @review = @material.reviews.build
    render 'new'
  end

  def newdev
    @material = Material.last
    @review = @material.reviews.build
    render 'new'
  end

  def destroy
  end

  private

    def review_params
      params.require(:review).permit(:grade_level_bottom, :grade_level_top, :time_required, :suggestions, :overall_rating, :content, :material_id)
    end



end

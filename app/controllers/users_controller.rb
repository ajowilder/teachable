class UsersController < ApplicationController
  before_action :logged_in_user, only: [:edit, :update, :index, :destroy, :following, :followers]
  before_action :correct_user, only: [:edit, :update]
  before_action :admin_user, only: [:destroy]

  def index
    @users = User.where(activated: true).paginate(page: params[:page])
  end

  def show
    @user = User.find(params[:id])
    @materials = @user.materials.paginate(page: params[:page])
  end

  def new
    @user = User.new
    @subjects = Topic.where(ancestry: nil).map(&:name).sort
  end

  def create
    @user = User.new(user_params)
    topics = params[:topics]
    grades = params[:grades]
    if topics
      topics.each do |topic|
        topic_id = Topic.find_by(name: topic).id
        @user.topic_ids << topic_id
      end
    end
    if grades
      grades.each do |grade|
        @user.grades_taught << grade_convert(grade)
      end
    end
    if @user.save
      log_in @user
      @user.send_activation_email
      flash[:info] = "You are now logged in and ready to save and view materials.  Please check your email and activate your account in order to rate and upload materials."
      redirect_to @user
    else
      @subjects = Topic.where(ancestry: nil).map(&:name).sort
      render 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
    @subjects = Topic.where(ancestry: nil).map(&:name).sort
    grades = @user.grades_taught
    @grades = []
    grades.each do |grade|
      @grades << grade_reconvert(grade)
    end
    topic_ids = @user.topic_ids
    @subject_topics = {}
    @course_topics = []
    topic_ids.each do |topic_id|
      topic = Topic.find(topic_id)
      if topic.parent == nil
        @subject_topics[topic.name] = []
        topic.children.each do |child|
          @subject_topics[topic.name] << child.name
        end
      else
        @course_topics << topic.name
        if !@subject_topics.include?(topic.parent.name)
          @subject_topics[topic.parent.name] = []
          topic.parent.children.each do |child|
            @subject_topics[topic.parent.name] << child.name
          end
        end
      end
    end
    gon.subjects = @subject_topics
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      topics = params[:topics]
      grades = params[:grades]
      @user.topic_ids = []
      @user.grades_taught = []
      if topics
        topics.each do |topic|
          topic_id = Topic.find_by(name: topic).id
          @user.topic_ids << topic_id
        end
      end
      if grades
        grades.each do |grade|
          @user.grades_taught << grade_convert(grade)
        end
      end
      @user.save
      flash[:success] = "Profile updated"
      redirect_to home_path
    else
      render 'edit'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "user deleted"
    redirect_to users_path
  end

  def following
    @title = "Following"
    @user  = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "Followers"
    @user  = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end

  private

    # user_params prevent hackers from creating users with admin priveleges with command line calls to api
    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
    end

    # Before filters
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless @user == current_user
    end
    def admin_user
      @user = current_user
      flash[:danger] = "nice try prick" unless current_user && current_user.admin?
      redirect_to(root_url) unless current_user && current_user.admin?
    end



end

class TopicsController < ApplicationController
  before_action :set_topic, only: [:show, :edit, :update, :destroy]

  def index
    @subjects = Topic.where(ancestry: nil).order(:name)
  end

  def show
    @topic = Topic.find(params[:id])
    @name = @topic.name
    if !@topic.parent.nil? && !@topic.parent.parent.nil?
      @topic_name = @name
      @parent = @topic.parent
      @course = @parent.name
      @parent_parent = @parent.parent
      @subject = @parent_parent.name
      materials = Material.where(topic_id: @topic).order(:average_rating).reverse_order
    elsif !@topic.parent.nil? && @topic.parent.parent.nil?
      @course = @name
      @parent = @topic.parent
      @subject = @parent.name
      topics = @topic.children
      materials = Material.where(topic_id: topics).order(:average_rating).reverse_order
    else
      @subject = @name
      children = @topic.children
      topic_ids = children.map do |child|
        child.children.map(&:id)
      end
      materials = Material.where(topic_id: topic_ids.join(' ').split(' ')).order(:average_rating).reverse_order
    end
    @children = @topic.children.all.order(:name)
    @posts = @topic.posts.all
    @types = Material::Content_types
    @materials = {}
    @types.each do |type|
      @materials[type] = materials.where(content_type: type).take(5)
    end
  end

  def discussion
    @topic = Topic.find(params[:id])
    @posts = @topic.posts.all
    render 'discussion'
  end




  # for AJAX requests in forms.  Return alls children of a subject, course, or topic.  Use for select inputs because only handles precise requests
  def subtopic
    subject = Topic.find_by(name: params[:subject]);
    @subtopics = subject.children.map(&:name).sort
    render :json => @subtopics
  end

  # for AJAX requests to return all potential topics of a course as options as user types in topic input
  def topic_search
    course = Topic.find_by(name: params[:course])
    subject = course.parent
    topics = Topic.where("name ~* ?", params[:search])
    ancestry = subject.id.to_s + '/' + course.id.to_s
    @topics = topics.where(ancestry: ancestry)

    render :json => @topics
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_topic
      @topic = Topic.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def topic_params
      params.require(:topic).permit(:name)
    end
end

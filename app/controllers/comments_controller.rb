class CommentsController < ApplicationController
  before_action :logged_in_user

  def create
    @post = Post.find(params[:post_id])
    @comment = @post.comments.build(comment_params)
    @comment.user = current_user
    if @comment.save
      @postable = @post.postable
      @comments = @post.comments
      redirect_to @post
    else
      @postable = @post.postable
      @comments = @post.comments
      render @post
    end
  end

  def destroy
  end

  private

    def comment_params
      params.require(:comment).permit(:content)
    end

end

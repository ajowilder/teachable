class MaterialsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy, :new]
  before_action :correct_user,   only: :destroy

  def show
    @user = current_user
    @material = Material.find(params[:id])
    @posts = @material.posts.take(10)
    if @user
      recently_viewed = @user.recently_viewed
      recently_viewed = recently_viewed.unshift(params[:id]).take(5)
      @user.update_attribute(:recently_viewed, recently_viewed)
    end
    @reviews = @material.reviews.take(5)
    similars = Material.where(topic: @material.topic)
    if similars.length > 6
      similars2 = Material.where("topic_id = ? AND content_type = ?", @material.topic.id, @material.content_type)
      if similars2.length > 1
        similars = similars2
      end
    elsif similars.length == 1
      topic_ids = @material.topic.parent.children
      similars = Material.where(topic_id: topic_ids)
    end
    @similars = similars.reorder('average_rating DESC').take(5) - [@material]
  end

  def index
    @materials = Material.all.paginate(page: params[:page])
  end

  def new
    @material = current_user.materials.build
    @subjects = Topic.where(ancestry: nil).map(&:name).sort
    first_ancestry = Topic.find_by(name: @subjects[0]).id
    @courses = Topic.where(ancestry: first_ancestry).map(&:name).sort
    @top_keywords = Material::Top_keywords
  end

  def create
    topic = params[:material][:topic]
    course = params[:material][:course]
    if !topic.empty? && !Topic.find_by(name: topic)
      Topic.create!(name: topic, parent: Topic.find_by(name: course))
    end
    material_topic = Topic.find_by(name: topic);
    @material = current_user.materials.build(material_params)
    @material.topic = material_topic
    @material.grade_range = Range.new(grade_convert(params[:grade_level_bottom]), grade_convert(params[:grade_level_top]))
    @material.average_rating = 5
    if @material.save
      clean_keywords(@material)
      other_keywords(@material)
      redirect_to @material
    else
      @subjects = Topic.where(ancestry: nil).map(&:name).sort
      first_ancestry = Topic.find_by(name: @subjects[0]).id
      @courses = Topic.where(ancestry: first_ancestry).map(&:name).sort
      @top_keywords = Material::Top_keywords
      render 'new'
    end
  end

  def edit
    @material = Material.find(params[:id])
    if @material.update_attributes(material_params)
      redirect_to @material
    else
      render 'edit'
    end
  end

  def destroy
    @material.destroy
    flash[:success] = "Material deleted"
    redirect_to request.referrer || root_url
  end

  def search_form
    @subjects = Topic.where(ancestry: nil).map(&:name).sort
  end

  def search
    #search db for search terms if any.  uses pg_search gem
    if params[:search_terms] && !params[:search_terms].empty?
      search_query = ''
      search_query += ' ' + params[:search_terms]
      materials = Material.search_for(search_query)
    else
      materials = Material.all
    end

    #Filter db search for only the content_types selected
    if params[:topic] && !params[:topic].empty?
      topic_id = Topic.find_by(name: params[:topic]).id
      materials = materials.where(topic_id: topic_id)
    elsif params[:course] && !params[:course].empty?
      topic = Topic.find_by(name: params[:course])
      topic_ids = topic.children
      materials = materials.where(topic_id: topic_ids )
    elsif params[:subject] && !params[:subject].empty?
      topic = Topic.find_by(name: params[:subject])
      children = topic.children
      topic_ids = children.map do |child|
        child.children.map(&:id)
      end
      materials = materials.where(topic_id: topic_ids.join(' ').split(' '))
    end
    if params[:types] && !params[:types].empty?
      materials = materials.where(content_type: params[:types])
    end

    #Filter db search with grades selected
    if params[:grades] && !params[:grades].empty?
      grades = params[:grades].map do |grade|
        grade_convert(grade)
      end
      if  grades.min == grades.max
        query_range = grades.min
        materials = materials.where('grade_range @> ?::integer', query_range)
      else
        query_range = Range.new(grades.min, grades.max)
        materials = materials.where('grade_range && int4range(?,?)', grades.min, grades.max)
      end
    end

    #Filter db search for only the time contstraints selected
    min_time = params[:hours_min].to_i * 60 + params[:minutes_min].to_i
    max_time = params[:hours_max].to_i * 60 + params[:minutes_max].to_i
    if min_time && !(min_time == 0)
      materials = materials.where('time_required >= ?',min_time)
    end
    if max_time && !(max_time == 0)
      materials = materials.where('time_required <= ?',max_time)
    end

    # If topic or course selected in original search, returns list of courses and topics to view so can populate select tag and show original selection as selected
    if params[:subject] && !params[:subject].empty?
      @courses = Topic.find_by(name: params[:subject]).children.map(&:name).sort
    else
      @courses = []
    end

    if params[:course] && !params[:course].empty?
      @topics = Topic.find_by(name: params[:course]).children.map(&:name).sort
    else
      @topics = []
    end



    # Sort by different params is sort_options changed
    if params[:sort_options] && params[:sort_options] != "Search Ranking"
      if params[:sort_options] == "Average Rating"
        materials = materials.reorder('average_rating DESC')
      elsif params[:sort_options] == "Time Req. Asc."
        materials = materials.reorder(:time_required)
      elsif params[:sort_options] == "Time Req. Desc."
        materials = materials.reorder(:time_required).reverse_order
      elsif params[:sort_options] == "Newest first"
        materials = materials.reorder(:created_at).reverse_order
      elsif params[:sort_options] == "Oldest first"
        materials = materials.reorder(:created_at)
      end
    else
      # Average rating will be default sort parameter
      materials = materials.reorder('average_rating DESC')
    end
    @length = materials.length
    if materials.length > 100
      materials = materials.limit(100)
    end


    @materials = materials
    @subjects = Topic.where(ancestry: nil).map(&:name).sort
    @params = params
    gon.params = @params
    render 'search'
  end

  def discussion
    @material = Material.find(params[:id])
    @posts = @material.posts.all
  end

  private

    def material_params
      params.require(:material).permit(:summary, :time_required, :title, :content_type, :course, :subject, {files: []}, :link, {keywords: []}, :average_rating)
    end

    def correct_user
      @material = current_user.materials.find_by(id: params[:id])
      redirect_to root_url if @material.nil?
    end

    def clean_keywords(material)
      keywords = material.keywords
      keywords.each do |keyword, i|
        if keyword == "0"
          keywords.delete(keyword)
        end
      end
      material.update_attributes(keywords: keywords)
    end

    def other_keywords(material)
      others = params[:other_keywords].remove(',').split()
      others.push(material.topic.name, material.topic.parent.name, material.topic.parent.parent.name)
      others.each do |keyword|
        keyword.capitalize!
        material.keywords << keyword
      end
      material.save
    end



end

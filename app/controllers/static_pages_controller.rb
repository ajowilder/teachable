class StaticPagesController < ApplicationController
  def home
    if logged_in?
      @user = current_user
      grades = @user.grades_taught
      topic_ids = @user.topic_ids
      followings = @user.following
      if !grades.empty? || !topic_ids.empty?
        course_materials = Material.all
      end
      if !topic_ids.empty?
        topics = topic_ids.map do |id|
          topic = Topic.find(id)
          if topic.parent == nil
            children = topic.children
            ids = children.map do |child|
              child.children.map(&:id)
            end
            ids.join(' ').split(' ')
          elsif topic.parent.parent == nil
            ids= topic.children.ids
          else
            id
          end
        end
        topics = topics.join(' ').split(' ')
        topic_ids.each do |id|
          if topics.include?(id)
            topics = topics_ids
          end
        end
        course_materials = course_materials.where(topic_id: topics)
      end
      if !grades.empty?
        if grades.min == grades.max
          query_range = grades.min
          course_materials = course_materials.where('grade_range @> ?::integer', query_range)
        else
          query_range = Range.new(grades.min, grades.max)
          course_materials = course_materials.where('grade_range && int4range(?,?)', grades.min, grades.max)
        end
      end
      if grades.empty? && topic_ids.empty?
        @course_materials = nil
      else
        @course_materials = course_materials.order(:created_at).take(5)
      end
      if !followings.empty?
        @following_materials = Material.where(user: followings).order(:created_at).take(5)
      else
        @following_materials = nil
      end

      @recent_views = Material.find(@user.recently_viewed)
    end
  end

  def help
  end

  def about
  end

  def contact
  end

  def search
  end

end

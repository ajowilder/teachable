class PostsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]

  def create
    if params[:postable_type] == 'material'
      material = Material.find(params[:material_id])
      @post = material.posts.build(post_params)
    else
      topic = Topic.find(params[:topic_id])
      postable = topic
      @post = topic.posts.build(post_params)
    end
    @post.user = current_user
    if @post.save
      if params[:postable_type] == 'material'
        redirect_to material
      else
        redirect_to topic
      end
    else
      flash[:danger] = "Post not saved."
      if params[:postable_type] == 'material'
        redirect_to material
      else
        redirect_to topic
      end
    end
  end

  def show
    @post = Post.find(params[:id])
    @postable = @post.postable
    @comments = @post.comments.order(:created_at)
  end

  def destroy
  end


  private
  def post_params
    params.require(:post).permit(:content)
  end


end

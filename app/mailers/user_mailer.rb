class UserMailer < ApplicationMailer

  def account_activation(user)
    @user = user
    mail to: user.email, subject: "Activate your Teachable Account"
  end

  def password_reset(user)
    @user = user
    mail to: user.email, subject: "Teachable: Reset your password"
  end
end

class Material < ApplicationRecord
  belongs_to :user
  belongs_to :topic
  has_many :reviews, dependent: :destroy
  has_many :posts, as: :postable
  before_save { self.title.titleize }


  # Virtual attributes for use on 'new' form-control
  attr_accessor :subject, :course, :hours, :minutes


  # Sorts with newest first
  # default_scope -> { order(created_at: :desc) }


  # carrierwave uploader
  mount_uploaders :files, FileUploader

  validates :user_id, presence: true
  validates :title, presence: true
  validates :topic, presence: true
  validates :summary, length: { maximum: 1023 }

  # These are the fundamental categories with which materials are organized.  Users must select one when uploading.  Materials can be searched for specfic content types
  Content_types = ['Minor Assignment', 'Major Assignment', 'Slideshow', 'Image', 'Video', 'Website', 'Assessment', 'Reading', 'Other'].sort

  # These are checkboxes on the search and upload forms.  These are used to enable rapid search for specific types of materials
  Top_keywords = %w(Group Individual AP Honors ESL EC Interactive Hands-On Podcast Lab Game Manipulative)

  Grades = ['preK', 'K', '1st', '2nd', '3rd', '4th', '5th','6th', '7th', '8th','9th', '10th', '11th', '12th']


  # enable advanced searchs using pg_search gem
  include PgSearch
  pg_search_scope :search_for,
    against:
    [[:title, 'A'], [:summary, 'B'], [:keywords, 'B'], [:content_type, 'C'], [:files, 'D']],
    using:
      {tsearch: {
        prefix: true,
        dictionary: "english",
        any_word: true }}



  # class methods

  def yt_id
    url = self.link.split(/(vi\/|v%3D|v=|\/v\/|youtu\.be\/|\/embed\/)/)
    ytID = url[2]?url[2].split(/[^0-9a-z_\-]/i)[0]:url[0];
  end

  def grade_level
    if self.grade_range
      min = self.grade_range.min
      max = self.grade_range.max
      if min == -1
        min = "preK"
      elsif min == 0
        min = "K"
      else
        min = min.ordinalize
      end
      if max == -1
        max = "preK"
      elsif max == 0
        max = "K"
      else
        max = max.ordinalize
      end
      return "#{min} - #{max}"
    else
      return 0
    end
  end

  def topic_tree
    topic_tree = [self.topic.name, self.topic.parent.name, self.topic.parent.parent.name]
  end



end

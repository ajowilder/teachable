class Post < ApplicationRecord
  belongs_to :user
  belongs_to :postable, polymorphic: true
  has_many :comments, as: :commentable
  validates :content, presence: true
  default_scope -> { order(created_at: :desc) }
end

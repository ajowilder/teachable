class Topic < ApplicationRecord
  has_ancestry
  has_many :materials
  before_save { self.name.titleize }
  has_many :posts, as: :postable
end

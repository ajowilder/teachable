class Review < ApplicationRecord
  belongs_to :user
  belongs_to :material
  has_many :comments, as: :commentable

  # Virtual attributes to allow use of form helpers
  attr_accessor :hours, :minutes

  validates :user_id, presence: true
  validates :material_id, presence: true




  def grade_level
    if self.grade_level_top && self.grade_level_bottom
      min = self.grade_level_bottom
      max = self.grade_level_top
      if min == -1
        min = "preK"
      elsif min == 0
        min = "K"
      else
        min = min.ordinalize
      end
      if max == -1
        max = "preK"
      elsif max == 0
        max = "K"
      else
        max = max.ordinalize
      end
      return "#{min} - #{max}"
    else
      return "no reviews"
    end
  end
end

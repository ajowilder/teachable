class Upload < ActiveRecord::Base
  mount_uploader  :file, FileUploader
  belongs_to :material


  def to_jq_upload
    {
      "name" => read_attributes(:file),
      "url" => file.url,
      "thumbnail_url" => file.thumb.url,
      "size" => file.size,
      "delete_url" => upload_path(id),
      "upload_id" => id,
      "delete_type" => "DELETE"
    }.to_json
  end

end

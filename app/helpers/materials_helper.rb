module MaterialsHelper

  def get_yt_id(url)
    url = url.split(/(vi\/|v%3D|v=|\/v\/|youtu\.be\/|\/embed\/)/)
    return url[2]?url[2].split(/[^0-9a-z_\-]/i)[0]:url[0];
  end

  def time_convert(minutes)
    if minutes
      hours = ((minutes / 60).floor).to_s
      minutes = (minutes % 60).to_s
      if hours.to_i > 0
        time = hours + "h. " + minutes + "m."
      else
        time = minutes + "m."
      end
    else
      time = nil
    end
  end

  def grade_convert(grade)
    if grade == "preK"
      -1
    elsif grade == "K"
      0
    else
      grade.gsub(/\D/, '').to_i
    end
  end

  def grade_reconvert(grade)
    if grade == -1
      "preK"
    elsif grade == 0
      "K"
    else
      grade.ordinalize
    end
  end

  def grades_display(grade_array)
    grades = grade_array.map do |grade|
      grade_reconvert(grade)
    end
    grades.join(', ')
  end
  
end

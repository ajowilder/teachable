
module UsersHelper

  # Returns the Gravatar for the given user.
  def gravatar_for(user, size: 80)
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
    image_tag(gravatar_url, alt: user.name, class: "gravatar")
  end

  def user_topics(topic_ids_array)
    topics = topic_ids_array.map do |topic_id|
      topic = Topic.find(topic_id)
      link_to topic.name, topic
    end
    topics.join(', ').html_safe
  end
end

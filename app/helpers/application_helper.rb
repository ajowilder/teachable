module ApplicationHelper

  # Returns full title for page headers based on the title provided by the layout
  def full_title(page_title = '')
    base_title = "Teachable"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end


end

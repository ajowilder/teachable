Rails.application.routes.draw do
  # Static pages
  root 'static_pages#home'
  get '/home', to: 'static_pages#home'
  get '/help', to: 'static_pages#help'
  get '/about', to: 'static_pages#about'
  get '/contact', to: 'static_pages#contact'


  # User controller pages
  get '/signup', to: 'users#new'
  post '/signup', to: 'users#create'
  resources :users do
    member do
      get :following, :followers
    end
  end

  # Sessions routes
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'

  # Account Activation routes
  resources :account_activations, only: [:edit]

  # Password Resets routes
  resources :password_resets, only: [:new, :create, :edit, :update]

  # Materials resource routes
  get '/upload', to: 'materials#new'
  post '/upload', to: 'materials#create'
  get '/search', to: 'materials#search_form'
  post '/materials', to: 'materials#search'
  get '/materials', to: 'materials#search'
  resources :materials do
    member do
      get :discussion
    end
  end

  # Relationships routes: create (follow), destroy (unfollow)
  resources :relationships, only: [:create, :destroy]

  # Topics routes
  get '/subtopic', to: 'topics#subtopic'
  get '/topic_search', to: 'topics#topic_search'
  resources :topics do
    member do
      get :discussion
    end
  end

  # Reviews routes
  resources :reviews, only: [:create, :new, :destroy]
  post '/review', to: 'reviews#new'
  get '/review', to: 'reviews#newdev'

  # Posts routes
  resources :posts, only: [:create, :show, :destroy]

  # Comments routes
  resources :comments, only: [:create, :destroy]


end

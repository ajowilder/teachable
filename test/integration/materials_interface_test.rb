require 'test_helper'

class MaterialsInterfaceTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:alan)
  end

  test "material interface" do
    log_in_as(@user)
    get root_path
    assert_select 'div.pagination'
    assert_select 'input[type="file"]'
    # Invalid submission
    assert_no_difference 'Material.count' do
      post materials_path, params: { material: { summary: "" } }
    end
    assert_select 'div#error_explanation'
    # Valid submission
    summary = "This material really ties the room together"
    picture = fixture_file_upload('test/fixtures/TestPhoto.jpg', 'image/jpg')
    assert_difference 'Material.count', 1 do 
      post materials_path, params: { material: { summary: summary, picture: picture  } }
    end
    assert @user.materials.first.picture?
    assert_redirected_to root_url
    follow_redirect!
    assert_match summary, response.body
    # Delete post
    assert_select 'a', text: 'delete'
    first_material = @user.materials.paginate(page: 1).first
    assert_difference 'Material.count', -1 do
      delete material_path(first_material)
    end
    # Visit different user (no delete links)
    get user_path(users(:archer))
    assert_select 'a', text: 'delete', count: 0
  end

  # test "material interface" do
  #   log_in_as(@user)
  #   get root_path
  #   assert_select 'div.pagination'
  #   assert_select 'input[type="file"]'
  #   # Invalid submission
  #   post materials_path, material: { summary: "" }
  #   assert_select 'div#error_explanation'
  #   # Valid submission
  #   summary = "This material really ties the room together"
  #   picture = fixture_file_upload('test/fixtures/TestPhoto.jpg', 'image/jpg')
  #   assert_difference 'Material.count', 1 do
  #     post materials_path, material: { summary: summary, picture: picture }
  #   end
  #   assert @user.materials.first.picture?
  #   follow_redirect!
  #   assert_match summary, response.body
  #   # Delete a post.
  #   assert_select 'a', 'delete'
  #   first_material = @user.materials.paginate(page: 1).first
  #   assert_difference 'Material.count', -1 do
  #     delete material_path(first_material)
  #   end
  #   # Visit a different user.
  #   get user_path(users(:archer))
  #   assert_select 'a', { text: 'delete', count: 0 }
  # end
end

require 'test_helper'

class MaterialsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @material = materials(:orange)
  end

  test "should redirect create when not logged in" do
    assert_no_difference 'Material.count' do
      post materials_path, params: { material: { summary: "Lorem ipsum" } }
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Material.count' do
      delete material_path(@material)
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy for wrong material" do
    log_in_as(users(:alan))
    material = materials(:ants)
    assert_no_difference 'Material.count' do
      delete material_path(material)
    end
    assert_redirected_to root_url
  end
end

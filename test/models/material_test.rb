require 'test_helper'

class MaterialTest < ActiveSupport::TestCase
  def setup
    @user = users(:alan)
    @material = @user.materials.build(summary: "Lorem ipsum")
  end

  test "should be valid" do
    assert @material.valid?
  end

  test "user id should be present" do
    @material.user_id = nil
    assert_not @material.valid?
  end

  test "summary should be present" do
    @material.summary = "   "
    assert_not @material.valid?
  end

  test "summary should be at most 255 characters" do
    @material.summary = "a" * 256
    assert_not @material.valid?
  end

  test "order should be most recent first" do
    assert_equal materials(:most_recent), Material.first
  end

end

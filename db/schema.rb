# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171213144515) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "comments", force: :cascade do |t|
    t.text "content"
    t.bigint "user_id"
    t.string "commentable_type"
    t.bigint "commentable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["commentable_type", "commentable_id"], name: "index_comments_on_commentable_type_and_commentable_id"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "materials", force: :cascade do |t|
    t.text "summary"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "content_type"
    t.string "link"
    t.string "title"
    t.bigint "topic_id"
    t.string "files", default: [], array: true
    t.integer "time_required"
    t.text "keywords", default: [], array: true
    t.int4range "grade_range"
    t.float "average_rating"
    t.index ["content_type"], name: "index_materials_on_content_type"
    t.index ["grade_range"], name: "index_materials_on_grade_range"
    t.index ["keywords"], name: "index_materials_on_keywords"
    t.index ["time_required"], name: "index_materials_on_time_required"
    t.index ["topic_id"], name: "index_materials_on_topic_id"
    t.index ["user_id", "created_at"], name: "index_materials_on_user_id_and_created_at"
    t.index ["user_id"], name: "index_materials_on_user_id"
  end

  create_table "posts", force: :cascade do |t|
    t.text "content"
    t.bigint "user_id"
    t.string "postable_type"
    t.bigint "postable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["postable_type", "postable_id"], name: "index_posts_on_postable_type_and_postable_id"
    t.index ["user_id"], name: "index_posts_on_user_id"
  end

  create_table "relationships", force: :cascade do |t|
    t.integer "follower_id"
    t.integer "followed_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["followed_id"], name: "index_relationships_on_followed_id"
    t.index ["follower_id", "followed_id"], name: "index_relationships_on_follower_id_and_followed_id", unique: true
    t.index ["follower_id"], name: "index_relationships_on_follower_id"
  end

  create_table "reviews", force: :cascade do |t|
    t.text "content"
    t.bigint "user_id"
    t.bigint "material_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "overall_rating"
    t.integer "grade_level_top"
    t.integer "grade_level_bottom"
    t.integer "time_required"
    t.text "suggestions"
    t.index ["material_id"], name: "index_reviews_on_material_id"
    t.index ["user_id", "material_id", "created_at"], name: "index_reviews_on_user_id_and_material_id_and_created_at"
    t.index ["user_id"], name: "index_reviews_on_user_id"
  end

  create_table "topics", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "ancestry"
    t.index ["ancestry"], name: "index_topics_on_ancestry"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.string "remember_digest"
    t.boolean "admin", default: false
    t.string "activation_digest"
    t.boolean "activated", default: false
    t.datetime "activated_at"
    t.string "reset_digest"
    t.datetime "reset_sent_at"
    t.boolean "teacher"
    t.integer "grades_taught", default: [], array: true
    t.integer "topic_ids", default: [], array: true
    t.integer "recently_viewed", default: [], array: true
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  add_foreign_key "comments", "users"
  add_foreign_key "materials", "topics"
  add_foreign_key "materials", "users"
  add_foreign_key "posts", "users"
  add_foreign_key "reviews", "materials"
  add_foreign_key "reviews", "users"
end

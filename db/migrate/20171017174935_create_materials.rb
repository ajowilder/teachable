class CreateMaterials < ActiveRecord::Migration[5.1]
  def change
    create_table :materials do |t|
      t.text :summary
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :materials, [:user_id, :created_at]
  end
end

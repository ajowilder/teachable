class AddKeywordsToMaterials < ActiveRecord::Migration[5.1]
  def change
    add_column :materials, :keywords, :text, array: true, default: []
  end
end

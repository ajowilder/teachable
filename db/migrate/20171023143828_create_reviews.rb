class CreateReviews < ActiveRecord::Migration[5.1]
  def change
    create_table :reviews do |t|
      t.text :content
      t.references :user, foreign_key: true
      t.references :material, foreign_key: true

      t.timestamps
    end
    add_index :reviews, [:user_id, :material_id, :created_at]
  end
end

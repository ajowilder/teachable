class AddTeachingDetailsToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :grades_taught, :integer, array: true, default: []
    add_column :users, :topic_ids, :integer, array: true, default: []
  end
end

class AddPictureToMaterials < ActiveRecord::Migration[5.1]
  def change
    add_column :materials, :picture, :string
  end
end

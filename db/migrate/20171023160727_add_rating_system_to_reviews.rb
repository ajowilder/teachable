class AddRatingSystemToReviews < ActiveRecord::Migration[5.1]
  def change
    add_column :reviews, :overall_rating, :integer
    add_column :reviews, :grade_level_top, :integer
    add_column :reviews, :grade_level_bottom, :integer
    add_column :reviews, :time_required, :integer
    add_column :reviews, :suggestions, :text




  end
end

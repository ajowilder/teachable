class AddGradeRangeToMaterials < ActiveRecord::Migration[5.1]
  def change
    add_column :materials, :grade_range, :int4range
    add_index :materials, :grade_range
    add_index :materials, :time_required
    add_index :materials, :content_type
    add_index :materials, :keywords
  end
end

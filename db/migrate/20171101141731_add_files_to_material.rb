class AddFilesToMaterial < ActiveRecord::Migration[5.1]
  def change
    add_column :materials, :files, :string, array: true, default: []
    remove_column :materials, :picture
  end
end

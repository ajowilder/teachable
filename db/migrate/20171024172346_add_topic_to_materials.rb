class AddTopicToMaterials < ActiveRecord::Migration[5.1]
  def change
    add_reference :materials, :topic, foreign_key: true
  end
end

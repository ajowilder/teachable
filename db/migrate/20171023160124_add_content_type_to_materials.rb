class AddContentTypeToMaterials < ActiveRecord::Migration[5.1]
  def change
    add_column :materials, :content_type, :string
    add_column :materials, :link, :string

  end
end

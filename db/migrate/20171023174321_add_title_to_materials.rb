class AddTitleToMaterials < ActiveRecord::Migration[5.1]
  def change
    add_column :materials, :title, :string
  end
end

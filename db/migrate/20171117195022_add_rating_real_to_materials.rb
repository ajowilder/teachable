class AddRatingRealToMaterials < ActiveRecord::Migration[5.1]
  def change
    add_column :materials, :average_rating, :real
  end
end

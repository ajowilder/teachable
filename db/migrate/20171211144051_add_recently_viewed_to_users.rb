class AddRecentlyViewedToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :recently_viewed, :integer, array: true, default: []
  end
end

class AddDurationToMaterials < ActiveRecord::Migration[5.1]
  def change
    add_column :materials, :time_required, :integer
  end
end

# Creating fake users
User.create!( name: "Example User",
              email: "example@example.org",
              password: "password",
              password_confirmation: "password",
              admin: true,
              activated: true,
              activated_at: Time.zone.now,
              grades_taught: [].push((-1..12).to_a.sample).push((-1..12).to_a.sample))

99.times do |n|
  name = Faker::Name.name
  email = "example-#{n+1}@example.org"
  password = "password"
  User.create!( name: name,
                email: email,
                password: password,
                password_confirmation: password,
                activated: true,
                activated_at: Time.zone.now,
                teacher: true,
                grades_taught: [].push((-1..12).to_a.sample).push((-1..12).to_a.sample))
end

# Creating topics
topics = {
  "Science" => {
    "Biology" => ["Cells", "Metabolism", "Energy", "Genetics", "Evolution", "Anatomy", "Biochemistry", "Cell Divsion / Mitosis", "Cell Divsion / Meiosis", "Molecular Genetics", "Biotechnology", "Viruses", "Animals", "Plants", "Bacteria", "Behavior", "Enzymes", "DNA / RNA", "Ecology", "Taxonomy"],
    "Chemistry" => ["Significant Figures", "Experimental Error", "Chemical bonding", "Atomic Structure", "Oxidation Numbers", "Reaction Equilibrium", "Lewis Structures", "Stoichiometry", "Molecular Geometry", "Chemical Reactions", "Balance Equations", "Redox Reactions", "States of Matter", "Periodic Table", "Periodic Trends", "Solutions", "Gas Laws", "Acids and Bases", "Buffers", "Thermodynamics", "Reaction Kinetics", "Nuclear Chemistry"],
    "Physics" => ["Forces", "Momentum", "Electricity", "Magnetism", "Kinematics", "Optics", "Fluid Mechanics", "Thermodynamics", "Oscillations and Waves", "Nuclear Physics", "Relativity"],
    "Earth Science" => ["Mapping", "Plate Tectonics", "Volcanic Activity", "Mountain Building", "Earthquakes", "Minerals", "Types of Rocks", "Geologic History", "Earth Processes", "Freshwater", "Coastal Processes", "Oceanography", "Atmosphere",  "Climate", "Environmental Resources"],
    "Astronomy" => ["Law of Gravity", "Laws of Motion", "Spectrum and Basic Spectroscopy", "Terrestrial planets", "Mercury", "Venus", "Earth", "Mars", "Outer Frozen planets", "Jupiter", "Saturn", "Uranus", "Neptune", "Meteors", "Asteroids", "Comets", "Stars", "Sun", "Stellar Evolution",  "White Dwarfs", "Neutron stars", "Black Holes", "Milk Way Galaxy", "Local Groups", "Super Clusters", "Quasars"],
    "Biomedical Science" => [],
    "Marine Biology" => [],
    "Physical Science" => [],
    "Geology" => [],
    "Forensic Science" => []
  },
  "English" => {
    "High School Eng." => [],
    "Middle School Eng." => [],
    "Elem. School Eng." => [],
    "AP Eng. Language" => [],
    "AP Eng. Literature" => [],
    "American literature" => [],
    "British literature" => [],
    "Contemporary literature" => [],
    "Creative writing" => [],
    "Communication skills" => [],
    "Debate" => [],
    "Journalism" => [],
    "Literary analysis" => [],
    "Modern literature" => [],
    "Poetry" => [],
    "Technical writing" => [],
    "Works of Shakespeare" => [],
    "World literature" => []
  },
  "History and Social Science" => {
    "US History" => [],
    "US Government" => [],
    "World History" => [],
    "Civics" => [],
    "Geography" => [],
    "Macroeconomics" => [],
    "Microeconomics" => [],
    "European History" => [],
    "Psychology" => [],
    "Current Events" => [],
    "Law" => [],
    "Religious Studies" => [],
    "Sociology" => []
  },
  "Math" => {
    "Geometry" => [],
    "Calculus AB" => [],
    "Algebra 1" => [],
    "Algebra 2" => [],
    "Statistics" => [],
    "Computer Math" => [],
    "Consumer Math" => [],
    "Pre-Algebra" => [],
    "Trigonometry" => [],
    "Pre-Calculus" => []
  },
  "Foreign Languages" => {
    "Spanish" => [],
    "French" => [],
    "German" => [],
    "Japanese" => [],
    "Russian" => [],
    "Italian" => [],
    "Chinese" => [],
    "Latin" => [],
    "ASL" => [],
    "Hebrew" => [],
    "Korean" => [],
    "Arabic" => [],
    "Greek" => []
  },
  "Technology" => {
    "Game Design" => [],
    "Networking" => [],
    "Computer Science" => [],
    "App Development" => [],
    "Audio Production" => [],
    "Computer Repair" => [],
    "Film Production" => [],
    "Graphic Design" => [],
    "Animation" => [],
    "Programming" => [],
    "Web Design" => [],
    "Auto Repair" => [],
    "JROTC" => [],
    "Woodworking" => [],
    "Metalworking" => [],
    "Driver Education" => []
  },
  "Arts" => {
    "Art History" => [],
    "Music Theory" => [],
    "Studio Art" => [],
    "Ceramics" => [],
    "Drama" => [],
    "Choir" => [],
    "Dance" => [],
    "Concert Band" => [],
    "Marching Band" => [],
    "Orchestra" => [],
    "Jazz Band" => [],
    "Percussion" => [],
    "Theater Tech." => [],
    "Sculpture" => [],
    "Painting" => []
  },
  "Physical Education" => {
    "Aerobics" => [],
    "Dance" => [],
    "Gymnastics" => [],
    "Weight Training" => [],
    "Health" => []
  },
  "Business" => {
    "Accounting" => [],
    "Bus. Law" => [],
    "Bus. Management" => [],
    "Consumer Ed." => [],
    "Entrepreneurial Skills" => [],
    "Intro. to Bus." => [],
    "Marketing" => [],
    "Personal Finance" => []
  }
 }

topics.each_with_index do |(subject, sub_hash), i|
  Topic.create!(name: topics.keys[i], parent_id: nil)
  sub_hash.each_with_index do |(course, course_hash), j|
    sub_id = Topic.find_by(name: topics.keys[i]).id
    Topic.create!(name: sub_hash.keys[j], parent_id: sub_id)
    course_hash.each do |topic|
      course_id = Topic.find_by(name: sub_hash.keys[j]).id
      Topic.create!(name: topic, parent_id: course_id)
    end
  end
end



all_topics = Topic.all
less_topics = []
all_topics.each do |topic|
  if topic.depth == 2
    less_topics.push(topic.id)
  end
end

types =  Material::Content_types

links = ["https://www.youtube.com/watch?v=MEIXRLcC6RA&index=1&list=PLwL0Myd7Dk1F0iQPGrjehze3eDpco1eVz", "https://www.youtube.com/watch?v=cQPVXrV0GNA&index=2&list=PLwL0Myd7Dk1F0iQPGrjehze3eDpco1eVz", "https://www.youtube.com/watch?v=EtWknf1gzKo&list=PLwL0Myd7Dk1F0iQPGrjehze3eDpco1eVz&index=3"]



top_keywords = %w(Group Individual AP Honors ESL EC Interactive Hands-On Podcast Lab Game Manipulative)

# Creating fake materials and reviews
users = User.all
2000.times do
  title = Faker::Lorem.sentence(5)
  summary = Faker::Lorem.sentences(15).join(' ')
  keywords = [].push(top_keywords.sample).push(top_keywords.sample).push(top_keywords.sample)
  user = users.sample
  user.materials.create!(title: title, summary: summary, content_type: types.sample, topic_id: less_topics.sample, link: links.sample, keywords: keywords, grade_range: Range.new((-1..8).to_a.sample, (9..12).to_a.sample), time_required: (10..400).to_a.sample, average_rating: (10..50).to_a.sample.to_f / 10)
end

10.times do
  content = Faker::Lorem.sentences(5).join(' ')
  suggestions = Faker::Lorem.sentences(5).join(' ')
  Material.all.each do |material|
    material.reviews.create!(user_id: (1..100).to_a.sample, overall_rating: (1..5).to_a.sample, grade_level_top: (8..12).to_a.sample, grade_level_bottom: (1..7).to_a.sample, suggestions: suggestions, time_required: (10.. 400).to_a.sample, content: content, suggestions: suggestions)
  end
end

# Creating fake relationships
users = User.all
user = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }

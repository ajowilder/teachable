# TEACHABLE README

working name: TEACHABLE

author: Alan Wilder

Ruby version: 2.3.4

Rails version: 5.1.4

Teachable is a platform to organize curricular materials for school teachers.  Users will be able to upload and organize materials for use in their classroom.  Most importantly, users will be able to browse and rate the materials uploaded by other users.  

Future features will include message boards and discussion tools for learning materials and curricular topics.  

To see current features, log in with:
username: 'example@example.org'
password: 'password'  

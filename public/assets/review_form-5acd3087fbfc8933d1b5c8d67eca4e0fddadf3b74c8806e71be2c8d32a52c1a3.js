var hours = $('#review_hours')
var minutes = $('#review_minutes')

hours.on('change', timeInput);
minutes.on('change', timeInput);

function timeInput() {
  time = parseInt(hours.val() * 60) + parseInt(minutes.val());
  $('#review_time_required').val(time)
};


var bottom = $('#review_grade_level_bottom');
var topper = $('#review_grade_level_top');

bottom.on('change', function() {
  topper.empty();
  bottom_val = bottom.val();
  if (bottom_val == "preK") bottom_val = -1;
  if (bottom_val == "K") bottom_val = 0;
  $.each(newRange(parseInt(bottom_val), 12), function(key,value){
    topper.append($("<option></option>")
     .attr("value", value).text(value))
  });
  topper.val(bottom.val())
});


function newRange(bottom, top) {
  var object = {};
  for (i = 1, j = bottom; j <= top; i++, j++) {
    var bottom_val = j;
    if (j == -1) bottom_val = "preK";
    if (j ==  0) bottom_val = "K";
    object[`Option ${i}`] = bottom_val;
  };
  return object;
}
;

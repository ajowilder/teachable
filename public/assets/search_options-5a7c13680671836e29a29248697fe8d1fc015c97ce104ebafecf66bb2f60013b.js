var subject_field = $('#subject');
var topics_field = $('#topic');
var course_field = $('#course')


subject_field.bind('change', changeSubject);
course_field.bind('change', changeCourse);






// changes course options when subject field is changed
function changeSubject() {
  const subject = subject_field.val();
  if (!subject) return;
  $.ajax({
    url: "/subtopic",
    type: 'GET',
    data: "subject=" + subject
  }).done(function(data) {
    console.log(data);
    var courses = data;
    course_field.empty();
    var blank = document.createElement('option');
    blank.innerHTML = ' ';
    course_field.append(blank)
    courses.forEach(function(course, i) {
      var opt = document.createElement('option');
      opt.innerHTML = course;
      course_field.append(opt)
    })
  })
};

function changeCourse() {
  const course = course_field.val();
  if (!course) return;
  $.ajax({
    url: "/subtopic",
    type: 'GET',
    data: "subject=" + course
  }).done(function(data) {
    var topics = data.map(function(topic) { return topic.name}).sort();
    topics_field.empty();
    var blank = document.createElement('option');
    blank.innerHTML = ' ';
    topics_field.append(blank)
    topics.forEach(function(course, i) {
      var opt = document.createElement('option');
      opt.innerHTML = course;
      topics_field.append(opt)
    })
  })
}

// $(document).ready(function() {
//   if (gon.params.subject) {
//     subject_field.val(gon.params.subject);
//     changeSubject();
//     setTimeout(function () {
//       course_field.val(gon.params.course);
//       changeCourse();
//       setTimeout(function() {
//         topics_field.val(gon.params.topic);
//       }, 300)
//     }, 300);
//   }
// })

$(document).ready(function() {
  if (gon.params && gon.params.subject && !gon.params.course) {
    changeSubject();
}})



$(document).ready(function() {
  $('.navbar-form').remove();
})
;

var link_field = $('#material_link');
var file_field = $('#material_files');
var material_keywords = $('.keyword_item :input');

link_field.on('input', changeLink)
file_field.on('change', changeFile)


//Fills in summary from youtube links
function changeLink() {
  const summary_input = document.querySelector('#material_summary')
  const link_input = document.querySelector('#material_link');
  if (link_input.value.includes('youtu') && YouTubeGetID(link_input.value) != link_input.value) {
    videoID = YouTubeGetID(link_input.value);
    $.ajax({
        url: "https://www.googleapis.com/youtube/v3/videos?id=" + videoID + "&key=AIzaSyBy-7-_RULolZLlYKWg4KjqDLvv-yd2mfQ&part=snippet,contentDetails",
        dataType: "jsonp"
    }).done(function(data){
      if (data.items.length != 0) {
        var vidLength = convertTimeYT(data.items[0].contentDetails.duration);
        var title = $('#material_title').val();
        $('#material_title').val(title + ` (${vidLength})`)
        summary_input.value = "From the youtube description: " + data.items[0].snippet.description;
        setTimeout(resizeTextArea, 200);
      } else {
        summary_input.value = ''
      }
    }).fail(function() {
      return false
    })
  }
};

//Expands summary field to fit description from youtube
function resizeTextArea() {
  var t = document.querySelector('#material_summary');
  if (t) {
    var resize = t.scrollHeight > 120 ? t.scrollHeight : 120;
    t.style.height = resize + 'px';
  }
};

// Displays which files have been uploaded
function changeFile() {
  var files = Array.from(this.files);
  var preview_list = document.querySelector('.file_preview_list');
  var html = files.map(file => {
    return `<li> <ul class="file_preview_item"><li><img src='/assets/document.svg'></li><li>${ file.name }</li></ul> </li>`
  }).join('');
  preview_list.innerHTML = html;
}






$(document).ready(resizeTextArea)
;

var subject_field = $('#subject');
var topic_field = $('#topic');
var search_results = document.querySelector('div.search_results');
var course_field = $('#course')


subject_field.bind('change', changeSubject);
subject_field.bind('window.onload', changeSubject);
topic_field.on('input', topicSearch);
topic_field.on('keyup', topicSelect);


// clears course topic when course is changed
course_field.on('change', function() {
  search_results.innerHTML = '';
  topic_field.val('');
});

// changes course options when subject field is changed
function changeSubject() {
  search_results.innerHTML = '';
  topic_field.val('');
  const subject = this.value;
  if (!subject) return;
  $.ajax({
    url: "/subtopic",
    type: 'GET',
    data: "subject=" + subject
  }).done(function(data) {
    var courses = data.map(function(topic) { return topic.name});
    course_field.empty();
    var blank = document.createElement('option');
    blank.innerHTML = ' ';
    course_field.append(blank)
    courses.forEach(function(course, i) {
      var opt = document.createElement('option');
      opt.innerHTML = course;
      course_field.append(opt)
    })
  })
};

//queries database for topics when text is input
function topicSearch(e) {
  e.preventDefault();
  const searchInput = this.value;
  const course = course_field.val();
  if (!searchInput) {
    search_results.style.display = 'none';
    return;
  };
  search_results.style.display = 'block'
  search_results.innerHTML = ""
  $.ajax({
    url: "/topic_search",
    type: 'GET',
    data: "course=" + course + "&search=" + searchInput
  }).done(function(data) {
    var topics = data.map(function(topic){ return topic.name });
    var result_list = document.createElement('ul');
    if (topics.length) {
      var html = topics.map(function(topic) {
        return '<span class="search_result">' + topic + '</span>'
      }).join('');
      search_results.innerHTML = html;
    };
    var search_result = search_results.querySelectorAll('.search_result');
      search_result.forEach(function(result) { result.addEventListener('click', topicClick)});
  }).fail(function() {
    return false;
  });
  return false;
};

// envent handler added to each search result so they can be clicked
function topicClick(e) {
  topic_field.val(this.textContent);
  search_results.innerHTML = '';
  return false;
};

// Allows user to navigate topic search results with up and down arrows and enter key
function topicSelect(e) {
    // if they aren't pressing up (40) down (38) or enter  (13) we dont Care
    if(![38,40,13].includes(e.keyCode)) return;
    const activeClass = 'search_result-active';
    var current = search_results.querySelector(`.${activeClass}`);
    const items = search_results.querySelectorAll('.search_result')
    let next;
    if (e.keyCode === 40 && current) {
      next = current.nextElementSibling || items[0];
    } else if (e.keyCode === 40) {
      next = items[0];
    } else if (e.keyCode === 38 && current) {
      next = current.previousElementSibling || items[items.length -1];
    } else if (e.keyCode === 38) {
      next = items[items.length -1];
    } else if (e.keyCode === 13) {
      if (current) topic_field.val(current.textContent);
      search_results.innerHTML = '';
      return false;
    }
    if (current) current.classList.remove(activeClass);
    if (items.length != 0) next.classList.add(activeClass);
};

$(document).ready(function() {
  $('.navbar-form').remove();
})
;

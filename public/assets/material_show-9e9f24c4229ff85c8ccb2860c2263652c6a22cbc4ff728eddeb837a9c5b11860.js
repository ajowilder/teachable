$('#review_show').on('click', goToReviews)
$('#top_review_show').on('click', expandReviews)
$('#discussion_show').on('click', goToDiscussion)



function expandReviews() {
  $('.reviews_hidden').toggleClass('hidden')
}
function goToReviews() {
  $('.reviews_hidden').removeClass('hidden');
  $("html, body").animate({
        scrollTop: $('#reviews_div').offset().top - 30
  });
}
function goToDiscussion() {
  $("html, body").animate({
        scrollTop: $('#discussion_div').offset().top - 30
  });
}
;

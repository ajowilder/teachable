var hours = $('#review_hours')
var minutes = $('#review_minutes')

hours.on('change', timeInput);
minutes.on('change', timeInput);

function timeInput() {
  time = parseInt(hours.val() * 60) + parseInt(minutes.val());
  $('#review_time_required').val(time)
};


var bottom = $('#review_grade_level_bottom');
var topper = $('#review_grade_level_top');

bottom.on('change', function() {
  topper.empty();
  $.each(newRange(parseInt(bottom.val()), 12), function(key,value){
    topper.append($("<option></option>")
     .attr("value", value).text(value))
  });
  topper.val(12)
});


function newRange(bottom, top) {
  var object = {};
  for (i = 1, j = bottom; j <= top; i++, j++) {
    object[`Option ${i}`] = j;
  };
  return object;
}
;

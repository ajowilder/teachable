function uploadOptions(e) {
  const link_field = document.querySelector('div.material_link');
  const form_expand2 = document.querySelector('div.material_expand2');
  const file_field = document.querySelector('div.material_file');
  const link_input = link_field.querySelector('#material_link');
  form_expand2.classList.add('hidden');
  link_field.classList.add('hidden');
  file_field.classList.remove('hidden');
  link_input.value = '';
};

function linkOptions() {
  const link_field = document.querySelector('div.material_link');
  const file_field = document.querySelector('div.material_file');
  const file_input = file_field.querySelector('#material_file');
  const form_expand2 = document.querySelector('div.material_expand2');
  link_field.classList.remove('hidden');
  file_field.classList.add('hidden');
  form_expand2.classList.add('hidden');
  file_input.value = "";
};

function changeFile() {
  const form_expand2 = document.querySelector('div.material_expand2');
  const file_type = document.querySelector('div.file_content_types');
  const title_input = form_expand2.querySelector('#material_title')
  const file_input = document.querySelector('#material_file');
  const summary_input = form_expand2.querySelector('#material_summary');
  form_expand2.classList.remove('hidden');
  file_type.classList.remove('hidden');
  if (file_input.files.length != 0) {
    summary_input.value = file_input.files[0].name
    title_input.value = file_input.files[0].name
  }
};

function changeLink() {
  const form_expand2 = document.querySelector('div.material_expand2');
  const link_type = document.querySelector('div.link_content_types');
  const title_input = form_expand2.querySelector('#material_title')
  const summary_input = form_expand2.querySelector('#material_summary')
  const link_input = document.querySelector('#material_link');
  form_expand2.classList.remove('hidden');
  link_type.classList.remove('hidden');
  if (link_input.value.includes('youtu') && YouTubeGetID(link_input.value) != link_input.value) {
    videoID = YouTubeGetID(link_input.value);
    $.ajax({
        url: "https://www.googleapis.com/youtube/v3/videos?id=" + videoID + "&key=AIzaSyBy-7-_RULolZLlYKWg4KjqDLvv-yd2mfQ&fields=items(snippet(title,description))&part=snippet",
        dataType: "jsonp"
    }).done(function(data){
      if (data.items.length != 0) {
        title_input.value = data.items[0].snippet.title;
        summary_input.value = data.items[0].snippet.description;
      } else {
        title_input.value = '';
        summary_input.value = ''
      }
    }).fail(() => {
      title_input.value = '';
      summary_input.value = ''
    })
  } else {
    title_input.value = '';
    summary_input.value = '';
  }
};

var subject_field = $('#material_subject');
var course_field = document.querySelector('#material_course');
var topic_field = $('#material_');

subject_field.bind('change', changeTopic);
subject_field.bind('window.onload', changeTopic);

function changeTopic() {
  const subject = this.value;
  if (!subject) return;
  $.ajax({
    url: "/subtopic",
    type: 'GET',
    data: "subject=" + subject
  }).done(function(data) {
    var courses = data.map(topic => topic.name);
    course_field.options.length = 0;
    courses.forEach((course, i) => {
      var opt = document.createElement('option');
      opt.innerHTML = course;
      course_field.appendChild(opt)
    })
  })
};

var picture_upload = $('#material_picture')
picture_upload.bind('change',function() {
  var size_in_megabytes = this.files[0].size/1024/1024;
  if (size_in_megabytes > 5) {
    alert('Maximum file size is 5MB.  Please choose a smaller file.');
    this.value = null;
  }
});

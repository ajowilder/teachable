var content_type_field = $('#material_content_type')
var link_div = $('.material_link')
var subject_field = $('#material_subject');
var topic_field = $('#material_topic');
var search_results = document.querySelector('div.search_results');
var link_field = $('#material_link');
var title_field = $("#material_title")
var course_field = $('#material_course')
var link_button = $('#link_button');
var summary_field = $('#material_summary')
var hours = $('#material_hours')
var minutes = $('#material_minutes')


// displays link input when link button is clicked
link_button.on('click', function() {
  if (link_div.hasClass('hidden')) {
    link_div.removeClass('hidden')
  } else link_div.addClass('hidden');
});

// event handlers to call changelink, which does ajax to get website title
link_field.on('input', changeLink)

// Ask for duration if user selects video
content_type_field.on('change', function() {
  if (content_type_field.val() == 'Video') {
    $('.duration').removeClass('hidden')
  }
})

// converts hours and minutes into total minutes for time_duration hidden field
hours.on('change', timeInput);
minutes.on('change', timeInput);

// Event handlers for determining topic and topic parent
subject_field.bind('change', changeSubject);
subject_field.bind('window.onload', changeSubject);
topic_field.on('input', topicSearch);
topic_field.on('keyup', topicSelect);

// Will not submit form when user presses enter on inputs
link_field.on("keypress", function(e) {
    if (e.keyCode == 13) {
        return false;
    }
});
title_field.on("keypress", function(e) {
    if (e.keyCode == 13) {
        return false;
    }
});
topic_field.on("keypress", function(e) {
    if (e.keyCode == 13) {
        return false;
    }
});

// Ajax to get title from link, API request if youtube link
function changeLink() {
  const link_input = link_field.val();
  if (link_input.includes('youtu') && YouTubeGetID(link_input) != link_input) {
    videoID = YouTubeGetID(link_input);
    $.ajax({
        url: "https://www.googleapis.com/youtube/v3/videos?id=" + videoID + "&key=AIzaSyBy-7-_RULolZLlYKWg4KjqDLvv-yd2mfQ&part=snippet,contentDetails",
        dataType: "jsonp"
    }).done(function(data){
      if (data.items.length != 0) {
        var vidLength = convertTimeYT(data.items[0].contentDetails.duration).split(':');
        if (vidLength.length == 2) vidLength.unshift('0')
        $('#review_time_required').val(parseInt(vidLength[0]) * 60 + parseInt(vidLength[1]));
        content_type_field.val('Video')
        $('.duration').removeClass('hidden')
        hours.val(parseInt(vidLength[0]));
        minutes.val(parseInt(vidLength[1]))
        timeInput();
        summary_field.val("From the youtube description: " + data.items[0].snippet.description);
        title_field.val(data.items[0].snippet.title);
        setTimeout(resizeTextArea, 200);
      } else {
        summary_input.value = ''
      }
    }).fail(function() {
      return false
    })
  }
};

// converts hours and minutes into total minutes for time_duration hidden field
function timeInput() {
  time = parseInt(hours.val() * 60) + parseInt(minutes.val());
  $('#material_time_required').val(time)
};


// clears course topic when course is changed
course_field.on('change', function() {
  search_results.innerHTML = '';
  topic_field.val('');
});

// changes course options when subject field is changed
function changeSubject() {
  search_results.innerHTML = '';
  topic_field.val('');
  const subject = this.value;
  if (!subject) return;
  $.ajax({
    url: "/subtopic",
    type: 'GET',
    data: "subject=" + subject
  }).done(function(data) {
    var courses = data.map(function(topic) { return topic.name});
    course_field.empty();
    courses.forEach(function(course, i) {
      var opt = document.createElement('option');
      opt.innerHTML = course;
      course_field.append(opt)
    })
  })
};

//queries database for topics when text is input
function topicSearch(e) {
  e.preventDefault();
  const searchInput = this.value;
  const course = course_field.val();
  if (!searchInput) {
    search_results.style.display = 'none';
    return;
  };
  search_results.style.display = 'block'
  search_results.innerHTML = ""
  $.ajax({
    url: "/topic_search",
    type: 'GET',
    data: "course=" + course + "&search=" + searchInput
  }).done(function(data) {
    var topics = data.map(function(topic){ return topic.name });
    var result_list = document.createElement('ul');
    if (topics.length) {
      var html = topics.map(function(topic) {
        return '<span class="search_result">' + topic + '</span>'
      }).join('');
      search_results.innerHTML = html;
    };
    var search_result = search_results.querySelectorAll('.search_result');
      search_result.forEach(function(result) { result.addEventListener('click', topicClick)});
  }).fail(function() {
    return false;
  });
  return false;
};

// envent handler added to each search result so they can be clicked
function topicClick(e) {
  topic_field.val(this.textContent);
  search_results.innerHTML = '';
  return false;
};

// Allows user to navigate topic search results with up and down arrows and enter key
function topicSelect(e) {
    // if they aren't pressing up (40) down (38) or enter  (13) we dont Care
    if(![38,40,13].includes(e.keyCode)) return;
    const activeClass = 'search_result-active';
    var current = search_results.querySelector(`.${activeClass}`);
    const items = search_results.querySelectorAll('.search_result')
    let next;
    if (e.keyCode === 40 && current) {
      next = current.nextElementSibling || items[0];
    } else if (e.keyCode === 40) {
      next = items[0];
    } else if (e.keyCode === 38 && current) {
      next = current.previousElementSibling || items[items.length -1];
    } else if (e.keyCode === 38) {
      next = items[items.length -1];
    } else if (e.keyCode === 13) {
      if (current) topic_field.val(current.textContent);
      search_results.innerHTML = '';
      return false;
    }
    if (current) current.classList.remove(activeClass);
    if (items.length != 0) next.classList.add(activeClass);
};

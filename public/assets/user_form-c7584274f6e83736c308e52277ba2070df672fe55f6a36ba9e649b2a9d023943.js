var topics = $('.topic_checkbox input');



topics.on('change', function() {
  var expand1 = $('.expand1');
  var subject = this.value;
  if (sessionStorage.subject) {
    sessionStorage.removeItem(subject)
  } else {
    $.ajax({
      url: "/subtopic",
      type: 'GET',
      data: "subject=" + subject
    }).done(function(data) {
      var courses = JSON.stringify(data);
      sessionStorage.setItem(subject, courses);
      console.log(courses);
    })
  }  
})

function handleCourses(courses) {
  var course_div = document.querySelector('.user_courses');
  html = courses.split(',').map(function(course) {
    return `<div class="topic_checkbox course_checkbox">
      <input type="checkbox" name="topics[]" id="${course}" value="${course}">
      <label for="${course}">${course}</label>
    </div>`
  }).join('');
  course_div.innerHTML = html
  var courses = document.querySelectorAll('.course_checkbox input');
  courses.forEach(function(course) {
    course.addEventListener('change', newCourse )
  })
}

function newCourse() {
  current_val = $('#topics_taught').val();
  if (current_val.includes(this.value)) {
    var new_val = current_val.replace(this.value, '')
  } else {
    var new_val = current_val + ',' + this.value;
  }
  new_val = trimCommas(new_val);
  topic_field.val(new_val)
  console.log(new_val);
}

function trimCommas(str) {
  str = str.replace(',,', ',');
  if (str[0] === ',') str = str.substr(1);
  if (str[str.length - 1] === ',') str = str.substring(0, str.length - 1);
  return str;
}
;

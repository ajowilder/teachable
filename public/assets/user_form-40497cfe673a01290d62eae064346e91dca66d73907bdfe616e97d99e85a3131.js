var grades = $('.grade_checkbox input');
var grade_field = $('#grades_taught');
var topics = $('.topic_checkbox input');
var topic_field = $('#topics_taught');

grades.on('change', function() {
  var current_val = grade_field.val();

  if (current_val.includes(this.value)) {
    var new_val = current_val.replace(this.value, '')
  } else {
    var new_val = current_val + ',' + this.value;
  }
  new_val = trimCommas(new_val);
  grade_field.val(new_val)
  console.log(grade_field.val());
});

topics.on('change', function() {
  current_topics = topic_field.val();
  var expand1 = $('.expand1');
  var courses = $('#courses');
  var current_courses = $('#courses').val();

  if (current_topics.includes(this.value)) {
    current_topics = current_topics.replace(this.value, '');
    $.ajax({
      url: "/subtopic",
      type: 'GET',
      data: "subject=" + this.value
    }).done(function(data) {
      data.forEach(function(course) {
        current_courses = current_courses.replace(course, '');
        current_courses = trimCommas(current_courses);
        current_topics = current_topics.replace(course, '');
        current_topics =trimCommas(current_topics);
      });
      courses.val(current_courses);
      topic_field.val(current_topics);
      if (current_courses === '') {
        expand1.addClass('hidden');
      };
      handleCourses(current_courses);
    })
  } else {
    topic_field.val(trimCommas(current_topics + ',' + this.value));
    $.ajax({
      url: "/subtopic",
      type: 'GET',
      data: "subject=" + this.value
    }).done(function(data) {
      data.forEach(function(course) {
        current_courses = current_courses + ',' + course;
      });
      current_courses = trimCommas(current_courses)
      courses.val(current_courses);
      expand1.removeClass('hidden');
      handleCourses(current_courses);
    })
  };
})

function handleCourses(courses) {
  var course_div = document.querySelector('.user_courses');
  html = courses.split(',').map(function(course) {
    return `<div class="topic_checkbox course_checkbox">
      <input type="checkbox" name="topics[]" id="${course}" value="${course}">
      <label for="${course}">${course}</label>
    </div>`
  }).join('');
  course_div.innerHTML = html
  var courses = document.querySelectorAll('.course_checkbox input');
  courses.forEach(function(course) {
    course.addEventListener('change', newCourse )
  })
}

function newCourse() {
  current_val = $('#topics_taught').val();
  if (current_val.includes(this.value)) {
    var new_val = current_val.replace(this.value, '')
  } else {
    var new_val = current_val + ',' + this.value;
  }
  new_val = trimCommas(new_val);
  topic_field.val(new_val)
  console.log(new_val);
}

function trimCommas(str) {
  str = str.replace(',,', ',');
  if (str[0] === ',') str = str.substr(1);
  if (str[str.length - 1] === ',') str = str.substring(0, str.length - 1);
  return str;
}
;
